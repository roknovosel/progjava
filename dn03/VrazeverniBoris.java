import java.util.Scanner;

public class VrazeverniBoris {

	private static int stejeMete(boolean jeNedelja) {
		int stevec = 0;
		int vsota = 0;
		int stMetov = 0;
		int pike = 0;
		if(jeNedelja) {
			while(true) {

				pike = (int)(Math.random()*6) + 1;
				System.out.print(pike + " ");
				if(pike % 2 != 0) {
					stevec++;
				}
				stMetov++;
				if(stevec == 5) break;

				
			}

			return stMetov;
		} else {
			while(true) {

				pike = (int)(Math.random()*6) + 1;
				System.out.print(pike + " ");
				if(pike % 2 != 0) {
					stevec++;
				}
				stMetov++;
				if(stevec == 3) break;

				
			}
			return stMetov;
		}
	}

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.println("Vnesite stevilo dni: ");
		int stDni = s.nextInt();

		for(int i = 1; i <= stDni; i++) {
			if(i % 7 == 0) {
				System.out.print(i + ". dan: ");
				int stMet = stejeMete(true);
				System.out.print(" | Skupaj " + stMet + " metov");
				System.out.println();
			} else {
				System.out.print(i + ". dan: ");
				int stMet = stejeMete(false);
				System.out.print(" | Skupaj " + stMet + " metov");
				System.out.println();
			}
		}

	}
}