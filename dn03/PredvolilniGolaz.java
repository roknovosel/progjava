import java.util.Scanner;

public class PredvolilniGolaz {

	private static double cenaPogostitve(double cenaKosila, int stKosil, boolean vino){
		if(vino){
			return cenaKosila * stKosil * 1.5;
		}else{
			return cenaKosila * stKosil;
		}
	}
	
	private static boolean jeNarocenoVino(int i){
		if (i == 1) return true;
		else return false;
	}

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		
		System.out.println("Vnesi zalogo denarja:");
		double zaloga = s.nextDouble();
		
		double cenaKosila, znesek;
		int stKosil, vino;
		
		while(zaloga > 0){
		
			System.out.println("Vnesi stevilo kosil:");
			stKosil = s.nextInt();
			System.out.println("Vnesi ceno kosila:");
			cenaKosila = s.nextDouble();
			System.out.println("So gostje narocili vino? (1: da / 2: ne)");
			vino = s.nextInt();
			znesek = cenaPogostitve(cenaKosila, stKosil, jeNarocenoVino(vino));
			
			System.out.println("Skupna cena pogostitve znasa: " + znesek);
			
			zaloga -= znesek;
			
			if(zaloga < 0) {
				System.out.println("Zmanjkalo je denarja!");
				break;
			}
			
			System.out.println("Trenutna zaloga denarja: " + zaloga);
		}	
			
		
		
	}
}