import java.util.Scanner;

public class Potenca {
	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.println("Vnesite osnovo:");

		int osnova = s.nextInt();
		System.out.println("Vnesite eksponent:");

		int eksponent = s.nextInt();
		int rezultat = 1;

		if(eksponent == 0) rezultat = 1;
		else if(osnova >= 1 && eksponent >= 0) {
			for(int i = 1; i <= eksponent; i++) {
				rezultat = rezultat * osnova;
			}

		}
		System.out.println(osnova + " ^ " + eksponent + " = " + rezultat);
	}
}