import java.util.Scanner;

public class Postevanka1 {
	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);

		System.out.println("Vnesite celo stevilo:");
		int celoStevilo = s.nextInt();
		System.out.println("Vnesite pozitivno celo stevilo:");
		int stevilo = s.nextInt();

		if(stevilo > 0) {
			for(int i = 1; i <= stevilo; i++) {
				System.out.println(celoStevilo + " * " + i + " = " + (i * celoStevilo));
			}
		} else {
			System.out.println("Stevilo ni pozitivno!");
		}
	}
}