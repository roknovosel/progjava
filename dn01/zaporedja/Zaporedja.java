import java.util.Scanner;

public class Zaporedja {
	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);

		System.out.println("Vnesite začetno število:");
		int zacetnoStevilo = s.nextInt();
		System.out.println("Vnesite končno mejo:");
		int koncnaMeja = s.nextInt();
		System.out.println("Vnesite korak: ");
		int korak = s.nextInt();

		if(korak != 0) {
			if(zacetnoStevilo < koncnaMeja && korak > 0) {
				for(int i = zacetnoStevilo; i <= koncnaMeja; i = i + korak) {
					System.out.print(i + " ");
				}
			} else if(zacetnoStevilo > koncnaMeja && korak < 0) {
				for(int i = zacetnoStevilo; i >= koncnaMeja; i = i + korak) {
					System.out.print(i + " ");
				}
			} else {
				System.out.println("Predznak koraka ni usklajen z mejama.");
			}
		} else {
			System.out.println("Korak ne sme biti enak 0.");
		}

	}
}