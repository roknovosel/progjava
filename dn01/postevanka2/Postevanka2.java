import java.util.Scanner;

public class Postevanka2 {
	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.println("Vnesite celo stevilo:");
		int celoStevilo = s.nextInt();
		System.out.println("Vnesite zgornjo mejo:");
		int zgornjaMeja = s.nextInt();
		int zmnozek = 0;

		for(int i = 1; (i*celoStevilo) <= zgornjaMeja; i++) {
			System.out.println(celoStevilo + " * " + i + " = " + (celoStevilo*i));
		}
	}
}