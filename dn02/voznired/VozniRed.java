import java.util.Scanner;

public class VozniRed {
	public static void main(String[] args) {
	
		Scanner s = new Scanner(System.in);

		System.out.println("Vnesi uro zacetka voznje:");
		int uraZacetek = s.nextInt();
		System.out.println("Vnesi minuto zacetka voznje:");
		int minutaZacetek = s.nextInt();
		System.out.println("Vnesi uro konca voznje:");
		int uraKonec = s.nextInt();
		System.out.println("Vnesi minuto konca voznje:");
		int minutaKonec = s.nextInt();
		System.out.println("Vnesi interval: (v minutah)");
		int interval = s.nextInt();
		
		int zacetekVoznje = (uraZacetek * 60) + minutaZacetek;
		int konecVoznje = (uraKonec * 60) + minutaKonec;
		
		for(int i = zacetekVoznje; i <= konecVoznje; i = i + interval){
			System.out.print((i / 60) + ":" + (i%60));			
			if(i%60 == 0) System.out.print("0");
			System.out.print("  ");
		}
	}
}