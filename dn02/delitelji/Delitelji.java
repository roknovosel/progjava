import java.util.Scanner;

public class Delitelji {
	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);

		System.out.println("Vnesi celo število:");
		int deljenec = s.nextInt();
		
		for(int i = 1; i <= (deljenec); i++){
			if(deljenec % i == 0){
				System.out.print(i + " ");
			}
		}
	}
}