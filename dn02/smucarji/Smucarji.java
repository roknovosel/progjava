import java.util.Scanner;

public class Smucarji {
	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);

		System.out.println("Vnesite stevilo tekmovalcev:");
		int steviloSmucarjev = s.nextInt();
		int prviTek, drugiTek;
		int topRezultat = 0;
		int zacasniRezultat = 0;
		int stevilkaTekmovalca = 0;

		for(int i = 1; i <= steviloSmucarjev; i++) {
			System.out.println("Tekmovalec st. " + i);

			System.out.println("Vnesite cas v prvem teku:");
			prviTek = s.nextInt();

			if(prviTek == -1) {
				System.out.println("Tekmovalec je diskvalificiran.");
				continue;
			}

			System.out.println("Vnesite cas v drugem teku:");
			drugiTek = s.nextInt();

			if(drugiTek == -1) {
				System.out.println("Tekmovalec je diskvalificiran.");
				continue;
			}

			zacasniRezultat = prviTek + drugiTek;

			if(zacasniRezultat > topRezultat) {
				topRezultat = zacasniRezultat;
				stevilkaTekmovalca = i;
			}
		}

		System.out.println("Zmagal je tekmovalec st. " + stevilkaTekmovalca + " z casom " + topRezultat);
	}
}