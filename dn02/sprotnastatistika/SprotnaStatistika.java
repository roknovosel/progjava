public class SprotnaStatistika {
	public static void main(String[] args) {

		double a = 0;
		int i = 1;
		double vsota = 0;
		double vsotaKvadratov = 0;
		double max=0;
		double min=0;
		double povprecje = 0;
		double prviClen = 0;
		double drugiClen = 0;
		double staOdklon = 0;

		while(Double.isNaN(a) == false) {
			System.out.println("Vnesi število:");
			a = BranjePodatkov.preberiDouble();
			if(a < min) min = a;
			if(a > max) max = a;

			vsota = vsota + a;
			povprecje = vsota / i;
			vsotaKvadratov = vsotaKvadratov + Math.pow(a,2);

			prviClen = vsotaKvadratov / i;
			drugiClen = Math.pow(povprecje,2);

			staOdklon = Math.sqrt(prviClen - drugiClen);

			System.out.println(max + " | " + min + " | " + povprecje + " | " + staOdklon);

			i++;
		}



	}
}